#!/usr/bin/env bash

set -o pipefail
set -eu

superdir="$rootdir"/..
dbrodir="$superdir"/grne-db-ro

lemmatiser_repo=ssh://git@gitlab.com/alleycatcc-private/grne-lemmatiser
lemmatiser_dbfile=GreekLexiconLogeion-20131219.db
git_ssh_command='SSH_AUTH_SOCK= ssh -i /path/to/.ssh/id_rsa-grne-lemmata-deploy'
lemmata_repo=git@github.com:lucienvanbeek/woordenboekgrne.git

grne-init-lemmatiser () {
    local lemmatiserdir=$1
    chd "$lemmatiserdir"
    cmd git clone "$lemmatiser_repo" tmp
    cpa tmp/"$lemmatiser_dbfile" lemmatiser.db
    safe-rm-dir tmp
}

grne-init-lemmata-svn-example () {
    # svncredfile="$bindir"/lemma-svn-credentials
    local lemmatadir=$1
    local user
    local pass
    local svncmd=(svn co "$lemmata_repo" -r "$lemmata_repo_rev")
    chd "$lemmatadir"
    if [ -e "$svncredfile" ]; then
        exec 3<"$svncredfile"
        read user <&3
        read pass <&3
        fun redirect-in <(printf "$pass") cmd "${svncmd[@]}" --username "$user" --password-from-stdin
    else
        cmd "${svncmd[@]}"
    fi
    cmd mv trunk/* .
    cmd rm -rf trunk
}

grne-init-lemmata () {
    local lemmatadir=$1
    chd "$lemmatadir"
    xport GIT_SSH_COMMAND "$git_ssh_command"
    cmd git clone "$lemmata_repo"
}
