#!/usr/bin/env bash

set -o pipefail
set -eu

rootdir="$bindir"/..

sourcesdirparts=("$rootdir" sources)
sourcesdir=$(join-out / sourcesdirparts)
grnesourcesdirstub=grne
grnesourcesdirparts=("$sourcesdir" "$grnesourcesdirstub")
grnebranchdefault=master
grneurl=https://gitlab.com/alleycatcc/grne
lemmatiserdir="$sourcesdir"/lemmatiser
lemmatadir="$sourcesdir"/lemmata
defaultmodes=(main search)
saxon9he_sourcesdirparts=("$sourcesdir" saxon9he)
saxon9he_sourcesdir=$(join-out / saxon9he_sourcesdirparts)
# --- the saxon https url at sourceforge is giving problems and breaking the
# build -- it's important to check the sha1 after downloading.
# saxon9he_remote=https://sourceforge.net/projects/saxon/files/Saxon-HE/9.9/SaxonHE9-9-1-2J.zip/download
saxon9he_remote=http://sourceforge.net/projects/saxon/files/Saxon-HE/9.9/SaxonHE9-9-1-2J.zip/download
saxon9he_name=SaxonHE9-9-1-2J.zip
saxon9he_sha1='85e7be92331ef83d399386dad3c9f56a7102f507 SaxonHE9-9-1-2J.zip'
# --- must match name in Dockerfile
cache_break_dummy_file="$rootdir"/.dummy

declare -A imagenames
imagenames[main]=grne-main
imagenames[search]=grne-search

declare -A dockerfiles
dockerfiles[main]=Dockerfile-main
dockerfiles[search]=Dockerfile-search

# --- given 'lemmatiserdir', must create a file 'lemmatiser.db' in
# it.

grne-init-lemmatiser () {
    error "Must override grne-init-lemmatiser()"
}

# --- given 'lemmatadir', must add .xml files under it, with an arbitrary
# hierarchy, as long as they are reachable using a `find` command.

grne-init-lemmata () {
    error "Must override grne-init-lemmata()"
}
