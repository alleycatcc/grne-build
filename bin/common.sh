#!/bin/bash

set -eu
set -o pipefail

. "$bindir"/vars.sh
. "$bindir"/vars-local.sh

check-modes () {
    local mode
    for mode in "$@"; do
        if [ "$mode" != 'search' -a "$mode" != 'main' ]; then
            error "$(printf "Invalid mode: %s" "$(bright-red "$mode")")"
        fi
    done
}

copy-array () {
    local tgt=$1
    local src=$2
    # --- eval is safe here because only the variable names are being
    # eval'ed, not the values.
    eval "$tgt=(\"\${$src[@]}\")"
}

array-length-out () {
    local aryname=$1
    local len
    eval "len=\${#$aryname[@]}"
    echo "$len"
}

