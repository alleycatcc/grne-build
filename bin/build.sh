#!/bin/bash

set -eu
set -o pipefail

bindir=$(realpath "$(dirname "$0")")

. "$bindir"/functions.bash

. "$bindir"/vars.sh
. "$bindir"/vars-local.sh
. "$bindir"/common.sh

USAGE_STR=(
    "Usage: $0 [-i] [-S] [-C] [step ...]"
    ''
    '-i = init sources'
    '-S = skip init saxon'
    '-G = skip getting grne sources'
    '-C to disable docker cache'
    '‘step ...’ defaults to ‘%s’'
)

USAGE=$(printf "$(join-out $'\n' USAGE_STR)" "${defaultmodes[*]}")

_ret=

opt_i=
opt_S=
opt_G=
opt_C=
opt_b="$grnebranchdefault"
while getopts hb:iSGC-: arg; do
    case $arg in
        h) warn "$USAGE"; exit 0 ;;
        b) opt_b=$OPTARG ;;
        i) opt_i=yes ;;
        S) opt_S=yes ;;
        G) opt_G=yes ;;
        C) opt_C=yes ;;
        -) OPTARG_VALUE="${OPTARG#*=}"
            case $OPTARG in
                help)  warn "$USAGE"; exit 0 ;;
                '')    break ;;
                *)     error "Illegal option --$OPTARG" ;;
                esac ;;
        *) error "$USAGE" ;;
    esac
done
shift $((OPTIND-1))

do_init=$opt_i
do_skip_get_saxon=$opt_S
do_skip_get_grne_sources=$opt_G

init-common () {
    mkd "$lemmatadir"
    fun grne-init-lemmata "$lemmatadir"
}

init-main () {
    mkd "$lemmatiserdir"
    fun grne-init-lemmatiser "$lemmatiserdir"
}

init-search () {
    true
}

init () {
    fun safe-rm-dir-array sourcesdirparts

    fun init-common
    fun init-main
    fun init-search
}

get-grne-sources () {
    local ret=$1; shift
    local grne_branch=$1
    local rev

    fun safe-rm-dir-array grnesourcesdirparts
    chd "$sourcesdir"
    cmd git clone "$grneurl" -b "$grne_branch" "$grnesourcesdirstub"
    chd "$grnesourcesdirstub"
    rev=$(git rev-parse --short HEAD)
    info "rev=$rev"
    cmd git submodule update --init --recursive
    chd config
    cmd ln -s config_build_example.py config_build.py
    read -d '' $ret <<< "$rev" || true
}

build () {
    local which=$1
    local imagename=$2
    local dockerfile=$3
    local rev=$4
    local nocache=${5:+--no-cache}

    local tag="$imagename:$rev"
    local taglatest="$imagename:latest"

    info "$(printf "build-%s → %s" "$which" "$(yellow "$imagename")")"

    # --- these env vars must exist, either having been set by jenkins or in
    # the shell.

    if [ "$which" = main ]; then
        fun redirect-out "$sourcesdir"/build-env cat <<EOT
        {
            "GRNE_REDIS_PERSIST_HOST": "$GRNE_REDIS_PERSIST_HOST",
            "GRNE_REDIS_PERSIST_PORT": "$GRNE_REDIS_PERSIST_PORT",
            "GRNE_REDIS_PERSIST_DB": "$GRNE_REDIS_PERSIST_DB",
            "GRNE_REDIS_PERSIST_PASSWORD": "$GRNE_REDIS_PERSIST_PASSWORD",
            "GRNE_REDIS_PERSIST_SOCKET_CONNECT_TIMEOUT": "$GRNE_REDIS_PERSIST_SOCKET_CONNECT_TIMEOUT"
        }
EOT
    fi
    cwd "$rootdir" cmd docker build $nocache -t "$tag" -f "$dockerfile" .
    cmd docker tag "$tag" "$taglatest"
}

generate-cache-break-dummy () {
    cmd redirect-out "$cache_break_dummy_file" echo "$RANDOM"
}

sha1check () {
    sha1sum -c
}

# --- the version in debian 9.6 is not high enough to run the T2 school
# transforms, so we download from sourceforge; would be good to use debian
# versions when they're ready and eliminate this step.

get-saxon () {
    fun safe-rm-dir-array saxon9he_sourcesdirparts
    mkd "$saxon9he_sourcesdir"
    chd "$saxon9he_sourcesdir"
    cmd curl -Lo "$saxon9he_name" "$saxon9he_remote"
    pipe sha1check echo "$saxon9he_sha1"
    cmd unzip "$saxon9he_name"
    chd-back
    cmd rm -f "$sourcesdir"/saxon9he.jar
    cpa "$saxon9he_sourcesdir"/saxon9he.jar "$sourcesdir"
}

begin () {
    local rev=$1; shift
    local nocache=$1; shift
    local modes
    local mode
    local imagename
    local dockerfile
    local builtnames=()
    local builtstring

    if [ $# = 0 ]; then modes=(all)
    else modes=("$@"); fi

    if [ "${modes[*]}" = all ]; then
        modes=("${defaultmodes[@]}")
    else
        fun check-modes "${modes[@]}"
    fi

    fun generate-cache-break-dummy

    for mode in "${modes[@]}"; do
        imagename=${imagenames["$mode"]}
        dockerfile=${dockerfiles["$mode"]}
        fun build "$mode" "$imagename" "$dockerfile" "$rev" "$nocache"
        builtnames+=("$imagename")
    done

    builtstring=$(join-out ', ' builtnames)
    info "$(printf "Successfully built %s" "$(green "$builtstring")")"
}

go () {
    local rev

    if [ "$do_init" = yes ]; then
        fun init
    fi

    if [ ! "$do_skip_get_grne_sources" = yes ]; then
        fun get-grne-sources _ret "$opt_b"
        rev=$_ret
    else
        rev=TMP
    fi

    # --- this can be disabled via an option in case it's useful to avoid
    # breaking the docker cache.

    if [ ! "$do_skip_get_saxon" = yes ]; then
        fun get-saxon
    fi

    fun begin "$rev" "$opt_C" "$@"
}; fun go "$@"
