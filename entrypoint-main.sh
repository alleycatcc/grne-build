#!/bin/bash

set -e

cmd () {
    echo "% $@"
    "$@"
}

info () {
    echo "* $@"
}

fun () {
    echo "$@" "()"
    "$@"
}

tail-with-tag () {
    local tag=$1
    local file=$2
    local line
    cmd tail -f "$file" | while read line; do
        echo "[$tag] $line"
    done
}

forkit () {
    echo "% [ fork ] $@"
    cmd "$@" &
}

grne_env=${GRNE_ENV:-prd}
nginx_dir=/etc/nginx
grne_dir=/grne/grne
grne_bin_dir="$grne_dir"/bin
grne_server_dir="$grne_dir"/server
wsgi_conf=/grne/grne/server/wsgi.deploy.ini
wsgi_num_processes=${GRNE_WSGI_NUM_PROCESSES:-}
wsgi_num_threads=${GRNE_WSGI_NUM_THREADS:-}

clear-nginx-conf () {
    cmd rm -f "$nginx_dir"/sites-enabled/*
}

enable-nginx-conf () {
    local which=$1
    cmd cp -f "$grne_server_dir"/nginx.deploy-"$which".conf "$nginx_dir"/sites-enabled
}

nginx-configure () {
    cmd rm -f "$nginx_dir"/nginx.conf
    cmd cp -f "$grne_server_dir"/nginx.conf "$nginx_dir"
}

wsgi-set () {
    local key=$1
    local val=$2
    cmd sed -i -e "s,^$key\s*=.\+,$key = $val," "$wsgi_conf"
}

wsgi-configure () {
    if [ -n "$wsgi_num_processes" ]; then
        fun wsgi-set processes "$wsgi_num_processes"
    fi
    if [ -n "$wsgi_num_threads" ]; then
        fun wsgi-set threads "$wsgi_num_threads"
    fi
}

start-nginx () {
    cmd service nginx restart
}

# ------ we use this redis server to store batch xml to html results for the
# pdf/docx download features, and for optionally implementing other
# app-level caches.

# --- start redis with a command -- redis runs as root and the owner of all
# resources is root.

redis-start-simple () {
    # --- forks.
    cmd redis-server /etc/redis/redis.conf
}

# --- start redis as a service -- redis runs as user 'redis' but this seems
# to cause problems with saving the .rdb file through a mounted drive,
# eventually failing to set any keys at all.

redis-start-service () {
    cmd chown redis:redis -R /var/log/redis
    cmd service redis-server start
}

redis-start () {
    fun redis-start-simple
}

# --- this is a 'fake' xserver, so we can use wkhtmltopdf for the pdf
# download feature.

# start-xvfb () {
# forkit Xvfb :1 -ac
# }

start-wsgi () {
    cmd "$grne_bin_dir"/start-wsgi.sh --mode=deploy
}

info "grne-env: $grne_env"

fun clear-nginx-conf
fun enable-nginx-conf "$grne_env"
fun nginx-configure
fun wsgi-configure
fun start-nginx
fun redis-start
# fun start-xvfb
fun forkit tail-with-tag 'nginx → access.log' /var/log/nginx/access.log
fun forkit tail-with-tag 'nginx → error.log' /var/log/nginx/error.log
fun forkit tail-with-tag 'redis → log' /var/log/redis/redis-server.log
fun start-wsgi
