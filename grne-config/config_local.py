# -*- coding: utf-8 -*-

import proj_paths # noqa

from acatpy.util import pathjoin

def get(sources_path):
    return {
        'lemma_input_path': '/sources/lemmata',
        'lemmatiser_path': '/sources/lemmatiser/lemmatiser.db',
        # --- for running the server locally; not necessary for normal
        # builds.
        # 'solr_url': 'http://alice:2302/solr/grne',
        # --- for using the cli analyse mode or bin/solr-search.py; not
        # necessary for normal builds.
        'solr_cli': {
            # 'tst': 'http://alice:2302/solr/grne',
            # 'acc': 'http://alice:2302/solr/grne',
        },
    }
